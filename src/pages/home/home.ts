import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Geolocation } from 'ionic-native';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  from: string;
  to: string;
  fromAutoComplete: any;
  toAutoComplete: any;
  busLocations: any;
  constructor(public navCtrl: NavController, private modalCtrl: ModalController) {
  }
  ngAfterViewInit() {
      let fromInput = document.getElementById('from').getElementsByTagName('input')[0];
      let toInput = document.getElementById('to').getElementsByTagName('input')[0];
      this.fromAutoComplete = new google.maps.places.Autocomplete(fromInput);
      this.toAutoComplete = new google.maps.places.Autocomplete(toInput);
  }
  ionViewDidLoad(){
    this.loadMap();
  }

  loadMap(){

    Geolocation.getCurrentPosition().then((position) => {
      this.busLocations = [{
          coords: {
            latitude: 10.4794354,
            longitude: -66.83370439999999
          }
      }, {
          coords: {
            latitude: 10.481988450081499,
            longitude: -66.84414625167847
          }
      }, {
        coords: {
          latitude: 10.4842742,
          longitude: -66.85342460000004
        }
      }]
      let stops = [{
        name: 'Av Francisco de Miranda, CC Los Ruices',
        coords: {
          latitude: 10.491894548925016,
          longitude: -66.829474568367
        }
      }, {
        name: 'Av Francisco de Miranda, metro Los Dos Caminos',
        coords: {
          latitude: 10.493719599636432,
          longitude: -66.83254301548004
        }
      }, {
        name: 'Av Francisco de Miranda, Parque del Este',
        coords: {
          latitude: 10.496103753708415,
          longitude: -66.83775722980499
        }
      }, {
        name: 'Av Francisco de Miranda, Metro de Altamira',
        coords: {
          latitude: 10.495491892912018,
          longitude: -66.84884011745453
        }
      }, {
        name: 'Av Francisco de Miranda, Multicentro Empresarial del Este',
        coords: {
          latitude: 10.493403117625784,
          longitude: -66.85370028018951
        }
      }, {
        name: 'Av Francisco de Miranda, Metro de Altamira',
        coords: {
          latitude: 10.495491892912018,
          longitude: -66.84884011745453
        }
      }, {
        name: 'Centro Comercial Expreso, Chacaito',
        coords: {
          latitude: 10.491493669291906,
          longitude: -66.8670791387558
        }
      }, {
        name: 'Colegio nuestra Sra de Fátima',
        coords: {
          latitude: 10.50789766138174,
          longitude: -66.85640394687653
        }
      }, {
        name: 'Cubo negro',
        coords: {
          latitude: 10.4842742,
          longitude: -66.85342460000004
        }
      }];
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let i = 0;
      let markers = [];
      let image = {
        url: 'https://d30y9cdsu7xlg0.cloudfront.net/png/98017-200.png',
        size: new google.maps.Size(75, 75),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(5, 34),
        scaledSize: new google.maps.Size(25, 25)
      };
      while(i < stops.length) {
        let mark = this.createMarker(stops[i], image);
        markers.push(mark);
        this.addInfoWindow(mark, stops[i]);
        i++;
      };
      this.createMarker({
        name: 'Actual Location',
        coords: position.coords
      }, null);
      let toAutoComplete = this.toAutoComplete;
      let map = this.map;
      let toMarker;
      google.maps.event.addListener(this.toAutoComplete, 'place_changed', function() {
        let place = toAutoComplete.getPlace();
        if (!place.geometry) {
          return;
        }
        if (place.geometry.viewport) {
          console.log('viewport', place.geometry.viewport);
        } else {
          toMarker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: place.geometry.location
          });
        }
      });
    }, (err) => {
      console.log(err);
    });
  }
  startSimulation() {
    let i = 0;
    let image = {
      url: 'http://205.166.161.233/MyRide/css/ico/bus_pointer.svg',
      size: new google.maps.Size(75, 75),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(5, 34),
      scaledSize: new google.maps.Size(30, 30)
    };
    let beforeMarker;
    while(i < this.busLocations.length) {
      setTimeout((map, bus, image) => {
        if(beforeMarker) {
          beforeMarker.setVisible(false);
        };
        beforeMarker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(bus.coords.latitude, bus.coords.longitude),
          icon: image
        });
      }, (i+1)*2000, this.map, this.busLocations[i], image);
      i++;
    };
  }
  createMarker(stop, image) {
    if(image) {
      return new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(stop.coords.latitude, stop.coords.longitude),
        icon: image
      });
    } else {
      return new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(stop.coords.latitude, stop.coords.longitude)
      });
    }
  }

  addInfoWindow(marker, stop) {
    let infoWindow = new google.maps.InfoWindow({
      content: stop.name
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  searchBus() {
    console.log(this.from, this.to);
    this.startSimulation();
  }
}
